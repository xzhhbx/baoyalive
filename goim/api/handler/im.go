/**
 * Created by lock
 * Date: 2019-10-06
 * Time: 23:40
 */
package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gochat/api/rpc"
	"gochat/proto"
	"gochat/tools"
)

type ChatList struct {
	Type       string `form:"type" json:"type" `
	AuthToken string `form:"authToken" json:"authToken" binding:"required"`
}

func GetchatList(c *gin.Context) {
	var chatList ChatList
	if err := c.ShouldBindBodyWith(&chatList, binding.JSON); err != nil {
		tools.FailWithMsg(c, err.Error())
		return
	}
	authToken := chatList.AuthToken

	checkAuthReq := &proto.CheckAuthRequest{AuthToken: authToken}
	code, fromUserId, fromUserName := rpc.RpcLogicObj.CheckAuth(checkAuthReq)
	_=fromUserId
	_=fromUserName
	if code == tools.CodeFail {
		tools.FailWithMsg(c, "rpc fail get self info")
		return
	}
	tools.SuccessWithMsg(c, "ok", nil)
	return
}
